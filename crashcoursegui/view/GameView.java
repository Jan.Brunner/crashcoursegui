package view;

import javax.swing.JPanel;

import model.grid.GridCell;
import model.grid.GridDimension;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;
import java.awt.Color;
import java.awt.Dimension;

public class GameView extends JPanel {

  private static final double OUTERMARGIN = 10;
  private static final double INNERMARGIN = 3;
  private ViewableModel model;

  // Constructor
  public GameView(ViewableModel model) {
    this.setFocusable(true);
    this.setPreferredSize(new Dimension(300, 400));
    this.model = model;
  }

  // The paintComponent method is called by the Java Swing framework every time
  // either the window opens or resizes, or we call .repaint() on this object.
  // Note: NEVER call paintComponent directly yourself
  @Override
  public void paintComponent(Graphics g) {
    super.paintComponent(g);
    Graphics2D g2 = (Graphics2D) g;
    drawGame(g2);
  }

  private void drawGame(Graphics2D g2) {
    Rectangle2D box = new Rectangle2D.Double(
        OUTERMARGIN,
        OUTERMARGIN,
        this.getWidth() - OUTERMARGIN * 2,
        this.getHeight() - OUTERMARGIN * 2);

    g2.setColor(Color.BLACK);
    g2.fill(box);

    CellPositionToPixelConverter converter = getCellPositionToPixelConverter();
    drawCells(g2, model.getIterable(), converter);
  }

  private void drawCells(Graphics2D g2, Iterable<GridCell<Color>> collection, CellPositionToPixelConverter converter) {
    for (GridCell<Color> cell : collection) {
      Rectangle2D rect = converter.getBoundsForCell(cell.pos());
      g2.setColor(cell.value() == null ? Color.DARK_GRAY : model.get(cell.pos()));
      g2.fill(rect);
    }
  }

  public CellPositionToPixelConverter getCellPositionToPixelConverter() {
    Rectangle2D bounds = new Rectangle2D.Double(
        OUTERMARGIN,
        OUTERMARGIN,
        this.getWidth() - 2 * OUTERMARGIN,
        this.getHeight() - 2 * OUTERMARGIN);
    GridDimension gridSize = this.model.getDimension();
    return new CellPositionToPixelConverter(bounds, gridSize, INNERMARGIN);
  }

}
