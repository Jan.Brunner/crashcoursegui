package controller;

import view.CellPositionToPixelConverter;
import view.GameView;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.geom.Point2D;

import model.grid.CellPosition;

public class GameController extends MouseAdapter {
    private ControllableModel model;
    private GameView view;

    public GameController(ControllableModel model, GameView view) {
        this.model = model;
        this.view = view;
        this.view.setFocusable(true);
        this.view.addMouseListener(this);
    }

    @Override
    public void mousePressed(MouseEvent event) {
        Point2D mouseCoordinate = event.getPoint();
        CellPositionToPixelConverter converter = this.view.getCellPositionToPixelConverter();
        CellPosition pos = converter.getCellPositionOfPoint(mouseCoordinate);
        this.model.set(pos);
        this.view.repaint();
    }
    
}
