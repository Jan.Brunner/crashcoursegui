package model;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class ColorGenerator {
    private List<Color> colors = new ArrayList<Color>(Arrays.asList(
        Color.RED, Color.BLUE, Color.ORANGE, Color.CYAN, Color.GREEN, Color.PINK
        ));
    private Random random = new Random();

    Color getRandom() {
        return colors.get(random.nextInt(colors.size()));
    }
}
