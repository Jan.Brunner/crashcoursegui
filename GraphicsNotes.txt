1. Opprett pakkene controller,model,view og lag klassen Main utenfor

2. Kopier grid-pakken fra Tetris inn i model

3. Opprett klassen colorGenerator;
    Lag en enkel Factory for den innebygde klassen java.awt.Color som gir en "tilfeldig" farge.​

​

4. Opprett en modellklasse som har en instans av denne fabrikken,
og et Grid med parametertypen java.awt.Color:
Grid<Color> color;
ColorGenerator colorGenerator;


Nå skal vi begynne å tilrettelegge for MVC, og vi skal benytte oss av restriktive
typer for modellen, ganske likt som i Tetris:
Lag pakkene view og controller

5. Lag grensesnittet ViewableModel, husker dere hva ViewableModel hadde tilgang på
i Tetris? Trenger vi flere?
Color get(CellPosition pos);
GridDimension getDimension();
Iterable<GridCell<Color>> getIterable();

6. La Model implementere grensesnittet

6B. Lag grensesnittet ControllableModel. Hvilke metoder trenger den?
Hva skal skje når vi trykker med musen?
metoden set(CellPosition pos)

6C. La Model implementere dette grensesnittet

6D.
7. Opprett klassen View
private static final double OUTERMARGIN = 10;
  private static final double INNERMARGIN = 3;
  private ViewableModel model;

this.model = model;
    this.colors = new DefaultColorTheme();
    this.setBackground(this.colors.getBackgroundColor());
    this.setFocusable(true);
    this.setPreferredSize(getDefaultSize(this.model.getDimension()));


8. Kopier kroppen til TetrisView inn i GameView og gjør følgende endringer:

Opprett denne metoden:
public CellPositionToPixelConverter getCellPositionToPixelConverter() {
    Rectangle2D bounds = new Rectangle2D.Double(
        OUTERMARGIN,
        OUTERMARGIN,
        this.getWidth() - 2 * OUTERMARGIN,
        this.getHeight() - 2 * OUTERMARGIN);
    GridDimension gridSize = this.model.getDimension();
    return new CellPositionToPixelConverter(bounds, gridSize, INNERMARGIN);
  }

Endre instansiereing av converteren til et kall til denne.

Fjern referanser til ColorTheme

g2.setColor(cell.value() == null ? Color.DARK_GRAY : model.get(cell.pos()));


9. I Main, legg til:
GameModel model = new GameModel();
        GameView view = new GameView(model);

JFrame frame = new JFrame();
frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
       frame.setContentPane(view);
        frame.pack();
        frame.setVisible(true);


Og kjør programmet, sjekk at en Grid vises.



11. Opprett klassen GameController, la den utvide MouseAdapter.
    private ControllableModel model;
    private GameView view;

    public GameController(ControllableModel model, GameView view) {
        this.model = model;
        this.view = view;
        this.view.setFocusable(true);
        this.view.addMouseListener(this);
    }



12. Overskriv MousePressed fra MouseAdapter:
    @Override
    public void mousePressed(MouseEvent event) {
        Point2D mouseCoordinate = event.getPoint();
        CellPositionToPixelConverter converter = this.view.getCellPositionToPixelConverter();
        CellPosition pos = converter.pointToPos(mouseCoordinate);
        this.model.set(pos);
        this.view.repaint();
    }

Opprett følgende metode i CellPositionToPixelConverter:
public CellPosition pointToPos(Point2D point) {
    // Same math as getBoundsForCell, but isolate the col/row on one side
    // and replace cellX with point.getX() (cellY with point.getY())
    double col = (point.getX() - box.getX() - margin) / (cellW + margin);
    double row = (point.getY() - box.getY() - margin) / (cellH + margin);

    return new CellPosition((int) row, (int) col);
}

13. Kjør programmet på nytt, og klapp deg selv på skulderen.


